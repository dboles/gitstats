set terminal svg \
	name "summary2" \
	size 1920, 1080 \
	background rgb "#000001" \
	noenhanced font ",10" \
	mousing;
set output "summary2.gp.out.svg";

the_datafile = "summary2.sh.out.txt";
set datafile separator "\t";
set xdata time;
set timefmt "%Y-%m-%d";


set multiplot layout 2, 1;
set tmargin 3;
set bmargin 0;
set lmargin 10;
set rmargin 10;
set border 11 linecolor rgb "#808080";

set autoscale fix;
set ytics  out textcolor rgb "white" nomirror;
set y2tics out textcolor rgb "white" nomirror;

set key \
	horizontal \
	reverse Left \
	textcolor rgb "white";


unset xtics;
set yrange [0:];
set ylabel "commits" textcolor rgb "white";
set y2range [0:];
set y2label "files"  textcolor rgb "white";

set style data boxes;
set offsets -43200, -43200, 0, 0;
set boxwidth 86400;
set style fill solid noborder;

set key \
	outside top center \
	title ARG1.(!(ARG2 eq "" || ARG2 eq ".") ? ": ".ARG2 : "") \
	textcolor rgb "white";

plot \
	the_datafile using "date":"commits" \
		title columnheader at 0.3, 0.97 \
		linecolor rgb "#0000ff", \
	"" using "date":"commits":( sprintf( "%s: %d commits", stringcolumn("date"), column("commits") ) ) \
		with labels \
		notitle \
		hypertext point \
		linecolor rgb "#ffffffff", \
	"" using "date":"files":( sprintf( "%s: %d files", stringcolumn("date"), column("files") ) ) \
		axes x1y2 \
		title columnheader at 0.4, 0.97 \
		with labels \
		hypertext point \
		pointtype 7 \
		linecolor rgb "#ff00ff";

set tmargin 2;
set bmargin 2;

set key title "";

set xtics out textcolor rgb "white" nomirror;
set format x "%Y-%m-%d";

unset yrange;
set autoscale yfix;
set offsets -43200, -43200, 0, graph 0.02;
set ylabel "lines elapsed" textcolor rgb "white";
unset y2range;
set autoscale y2fix;
set y2label "lines changed"  textcolor rgb "white";

plot \
	the_datafile using "date":"insertions" \
		axes x1y2 \
		title columnheader at 0.5, 0.97 \
		linecolor rgb "#00ff00", \
	"" using "date":"insertions":( sprintf( "%s: %d insertions", stringcolumn("date"), column("insertions") ) ) \
		axes x1y2 \
		with labels \
		notitle \
		hypertext point \
		linecolor rgb "#ffffffff", \
	"" using "date":"deletions" \
		axes x1y2 \
		title columnheader at 0.6, 0.97 \
		linecolor rgb "#ff0000", \
	"" using "date":"deletions":( sprintf( "%s: %d deletions", stringcolumn("date"), column("deletions") ) ) \
		with labels \
		axes x1y2 \
		notitle \
		hypertext point \
		linecolor rgb "#ffffffff", \
	"" using "date":"lines" \
		with steps \
		title columnheader at 0.7, 0.97 \
		linecolor rgb "#ffff00", \
	"" using "date":"lines":( sprintf( "%s: %d lines", stringcolumn("date"), column("lines") ) ) \
		with labels \
		title " " at 0.7, 0.97 \
		hypertext point \
		pointsize 0.5 \
		linecolor rgb "#ffff00";
