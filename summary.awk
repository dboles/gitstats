BEGIN {
	RS = "";
	FS = "[\n\t]";

	OFS = "\t";
}

{
	date = $1;

	++commits[date];

	insertions = 0;
	deletions  = 0;

	for (i = 2; i < NF;) {
		insertions += $i == "-" ? 0 : $i;
		++i;

		deletions  -= $i == "-" ? 0 : $i;
		++i;

		files[date][$i] = 1;
		++i;
	}

	sum_insertions[date] += insertions;
	sum_deletions [date] += deletions ;
}

END {
	PROCINFO["sorted_in"] = "@ind_str_asc";

	for (date in commits) {
		insertions = sum_insertions[date];
		deletions  = sum_deletions [date];
		lines += insertions + deletions;

		print date, commits[date], length( files[date] ), insertions, deletions, lines;
	}
}
