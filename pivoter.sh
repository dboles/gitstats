#!/bin/bash

source source/git_path.sh
source source/log_paths.sh

{
	echo 'date	hash	file	insertions	deletions'

	git -C "$git_path" log --no-renames --numstat --date=short --pretty=format:'%ad	%h' \
	|
	gawk -f pivoter.awk \
	|
	LC_ALL=C sort
}
