#!/bin/bash

source source/git_path.sh

{
	echo 'date	commits	insertions	deletions	lines'

	IFS=$'\n'
	for file in $(git -C "$git_path" ls-files $@)
	do
		echo
		echo
		echo $file

		git -C "$git_path" log --follow --numstat --date=short --pretty=format:%ad -- "$file" \
		|
		gawk -f files.awk
	done
} \
> files.sh.out.txt

unset IFS
gnuplot -c files.gp "$git_path" "$*"
