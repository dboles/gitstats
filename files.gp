set terminal svg \
	name "files" \
	size 1920, 1080 \
	background rgb "#000001" \
	noenhanced font ",10" \
	mousing;
set output "files.gp.out.svg";

set datafile separator "\t";
stats "files.sh.out.txt" skip 1 using 2 nooutput;

set key \
	outside top center \
	title ARG1.(!(ARG2 eq "" || ARG2 eq ".") ? ": ".ARG2 : "") textcolor rgb "white" \
	horizontal \
	reverse Left \
	autotitle columnheader textcolor rgb "white";

set border 3 linecolor rgb "#808080";
set tics out textcolor rgb "white" nomirror;
set autoscale fix;
set offsets graph 0.01, 0, 0, 0;

set xdata time;
set timefmt "%Y-%m-%d";
set format x "%Y-%m-%d";

set ylabel "lines elapsed" textcolor rgb "white";
set yrange [0:];

get_colour(i) = hsv2rgb( (i - 1.0) / STATS_blocks, 1, 1 );

plot \
	for [i = 1:STATS_blocks] \
		"files.sh.out.txt" \
		index i \
		using 1:5 \
		notitle \
		with steps \
		linecolor rgb get_colour(i), \
	for [i = 1:STATS_blocks] \
		"" \
		index i \
		using 1:5:( sprintf("%s\n%s\n%d commits\n%d insertions, %d deletions\n%d lines", \
		                    columnhead(1), stringcolumn(1), $2, $3, $4, $5) ) \
		with labels \
		hypertext point \
		pointtype 5 + 2 * ( (i - 1) % 6 ) \
		pointsize (2 / 3.0) \
		linecolor rgb get_colour(i);
