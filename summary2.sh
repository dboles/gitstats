#!/bin/bash

source source/git_path.sh
source source/log_paths.sh

{
	echo 'date	commits	files	insertions	deletions	lines'

	git -C "$git_path" log --no-renames --numstat --date=short --pretty=format:'%ad' $log_paths \
	|
	gawk -f summary.awk
} \
> summary2.sh.out.txt

gnuplot -c summary2.gp "$git_path" "$log_paths"
