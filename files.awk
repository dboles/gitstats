BEGIN {
	RS = "";
	FS = "[\t\n]";

	OFS = "\t";
}

{
	date = $1;
	++commits[date];

	insertions = $2 == "-" ? 0 : +$2;
	deletions  = $3 == "-" ? 0 : -$3;

	sum_insertions[date] += insertions;
	sum_deletions [date] += deletions ;
}

END {
	PROCINFO["sorted_in"] = "@ind_str_asc";

	for (date in commits) {
		insertions = sum_insertions[date];
		deletions  = sum_deletions [date];
		lines += insertions + deletions;

		print date, commits[date], insertions, deletions, lines;
	}
}
