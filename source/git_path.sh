if test $# -ge 1
then
	git_path="$1"
	shift

	if test $git_path = "."
	then
		git_path="$PWD"
	fi
else
	git_path="$PWD"
fi
