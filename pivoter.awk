BEGIN {
	RS = "";
	FS = "[\n\t]";

	OFS = "\t";
}

{
	date = $1;
	hash = $2;

	for (i = 3; i < NF;) {
		insertions = $i == "-" ? 0 : +$i;
		++i;

		deletions  = $i == "-" ? 0 : -$i;
		++i;

		file = $i;
		++i;

		print date, hash, file, insertions, deletions;
	}
}
