set terminal svg \
	name "summary4" \
	size 1920, 1080 \
	background rgb "#000001" \
	noenhanced font ",10" \
	mousing;
set output "summary4.gp.out.svg";

the_datafile = "summary4.sh.out.txt";
set datafile separator "\t";
set xdata time;
set timefmt "%Y-%m-%d";


set multiplot layout 4, 1;
set bmargin 2;
set lmargin 7;
set rmargin 2;
set border 3 linecolor rgb "#808080";

set autoscale fix;
set ytics out textcolor rgb "white" nomirror;

set key \
	horizontal \
	reverse Left \
	textcolor rgb "white";


unset xtics;
set yrange [0:];

set style data boxes;
set style fill solid noborder;
set boxwidth 86400;

set tmargin 3;
set key \
	outside top center \
	title ARG1.(!(ARG2 eq "" || ARG2 eq ".") ? ": ".ARG2 : "") \
	textcolor rgb "white";

set offsets -43200, -43200, 0, 0;

plot \
	the_datafile using "date":"commits" \
		title columnheader at 0.3, 0.97 \
		linecolor rgb "#00ffff", \
	"" using "date":"commits":( sprintf( "%s: %d commits", stringcolumn("date"), column("commits") ) ) \
		with labels \
		notitle \
		hypertext point \
		linecolor rgb "#ffffffff";

set tmargin 0;
set key title "";

plot \
	the_datafile using "date":"files" \
		title columnheader at 0.4, 0.97 \
		linecolor rgb "#ff00ff", \
	"" using "date":"files":( sprintf( "%s: %d files", stringcolumn("date"), column("files") ) ) \
		with labels \
		notitle \
		hypertext point \
		linecolor rgb "#ffffffff";

unset yrange;
set autoscale yfix;

plot \
	the_datafile using "date":"insertions" \
		title columnheader at 0.5, 0.97 \
		linecolor rgb "#00ff00", \
	"" using "date":"insertions":( sprintf( "%s: %d insertions", stringcolumn("date"), column("insertions") ) ) \
		with labels \
		notitle \
		hypertext point \
		linecolor rgb "#ffffffff", \
	"" using "date":"deletions" \
		title columnheader at 0.6, 0.97 \
		linecolor rgb "#ff0000", \
	"" using "date":"deletions":( sprintf( "%s: %d deletions", stringcolumn("date"), column("deletions") ) ) \
		with labels \
		notitle \
		hypertext point \
		linecolor rgb "#ffffffff";

set xtics out textcolor rgb "white" nomirror;
set format x "%Y-%m-%d";
set offsets 43200, 43200, 0, graph 0.04;

plot \
	the_datafile using "date":"lines" \
		with steps \
		title columnheader at 0.7, 0.97 \
		linecolor rgb "#ffff00", \
	"" using "date":"lines":( sprintf( "%s: %d lines", stringcolumn("date"), column("lines") ) ) \
		with labels \
		title " " at 0.7, 0.97 \
		hypertext point \
		pointsize 0.5 \
		linecolor rgb "#ffff00";
